/**
 * node
 * by lotalorafox
 */
import java.util.*;

public class Node {
    public String name;
    public int value;
    public HashMap<Node,Integer> in;
    public HashMap<Node,Integer> out;
    HashMap<Integer,String> srtwayshr = new HashMap<>();
    HashMap<Integer,String> srtwaylon = new HashMap<>();

    public Node(String n){
        this.name = n;
        in = new HashMap<>();
        out = new HashMap<>();
    }
    public void addin(Node a, int cost){
        if(a != null)
            in.put(a, cost);
    }
    public void addout(Node a, int cost){
        if(a != null)
            out.put(a, cost);
    }
    public int getcostin(Node a){
        if(in.containsKey(a)){
            return in.get(a);
        }
        return -1;
    }
    public int getcostout(Node a){
        if(out.containsKey(a)){
            return out.get(a);
        }
        return -1;
    }
    public String getInString(){
        System.out.println("in:");
        return this.in.toString();
    }
    public String getOutString(){
        System.out.println("out:");
        return this.out.toString();
    }
    public int findshorterway(Node a){
        //System.out.println(this.name);
        ArrayList<Integer> ways = new ArrayList<>();
        if(this.out.containsKey(a)){
            //System.out.print("one step");
            ways.add(this.out.get(a));
            srtwayshr.put(this.out.get(a),"");
        }else if(a.name.equals(this.name)){
            return 0;
        }
        if(this.out.size() == 0){
            return -500000;
        }
        Object[] k = this.out.keySet().toArray();
        for(int i =0;i<out.size();i++){
            String str = this.name;
            int lo = this.out.get((Node)k[i]);
            if(!(((Node)k[i]).name.equals(a.name))){
                str += " " +((Node)k[i]).name;
            }
            //System.out.println("lo " + lo);
            lo +=((Node)k[i]).findshorterway(a);
            //str += ((Node)a).name;
            if(srtwayshr.containsKey(lo)){
                srtwayshr.replace(lo, str);
            }else{
                srtwayshr.put(lo, str);
            }
            //System.out.println(this.srtwayshr);
            ways.add(lo);
        }
        Collections.sort(ways);
        //System.out.println(this.name +" " + ways);
        for(int i=0;i<ways.size();i++){
            if(ways.get(i) > 0){
                return ways.get(i);
            }
        }
        return -1;
    }
    public int findlongestway(Node a){
        //System.out.println(this.name);
        ArrayList<Integer> ways = new ArrayList<>();
        if(this.out.containsKey(a)){
            ways.add(this.out.get(a));
            srtwayshr.put(this.out.get(a),"");
        }else if(a.name.equals(this.name)){
            return 0;
        }
        if(this.out.size() == 0){
            return -500000;
        }
        
        HashMap<Integer,String> srtway = new HashMap<>();
        Object[] k = this.out.keySet().toArray();
        for(int i =0;i<out.size();i++){
            String str = this.name;
            int lo = this.out.get((Node)k[i]);
            if(!(((Node)k[i]).name.equals(a.name))){
                str += " " +((Node)k[i]).name;
            }
            //System.out.println("lo " + lo);
            lo +=((Node)k[i]).findshorterway(a);
            //str += ((Node)a).name;
            if(srtwaylon.containsKey(lo)){
                srtwaylon.replace(lo, str);
            }else{
                srtwaylon.put(lo, str);
            }
            ways.add(lo);
        }
        Collections.sort(ways);
        //System.out.println(this.name +" " + ways);
        for(int i=ways.size()-1;i>=0;i--){
            if(ways.get(i) > 0){
                //System.out.println("route to the longest way " + srtway.get(ways.get(i)) + " " + a.name);
                return ways.get(i);
            }
        }
        return -50000000;
    }
}