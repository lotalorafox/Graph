import java.util.ArrayList;
import java.util.HashMap;
/**
 * Graph
 * by lotalorafox
 */
public class Graph {
    ArrayList<Node> nodes;
    public Graph(){
        nodes = new ArrayList<>();
    }
    public Graph(Node a){
        nodes = new ArrayList<>();
        nodes.add(a);
    }
    public void addnode(Node a){
        if(!(this.containsnode(a))){
            this.nodes.add(a);
        }else{
            int i = this.containsnodeindex(a);
            this.nodes.get(i).in.putAll(a.in);
            this.nodes.get(i).out.putAll(a.out);
        }
        
    }
    public boolean containsnode(Node a){
        for(int i=0;i<nodes.size();i++){
            if(nodes.get(i).name.equals(a.name)){
                return true;
            }
        }
        return false;
    }
    public int containsnodeindex(Node a){
        for(int i=0;i<nodes.size();i++){
            if(nodes.get(i).name.equals(a.name)){
                return i;
            }
        }
        return -1;
    }
    public int containsnodeindex(String a){
        for(int i=0;i<nodes.size();i++){
            if(nodes.get(i).name.equals(a)){
                return i;
            }
        }
        return -1;
    }

    public void remove(String l){
        for(int i=0;i<nodes.size();i++){
            if(nodes.get(i).name.equals(l))
                nodes.remove(i);
        }
    }
    public void printnodes(){
        for(int i=0;i<nodes.size();i++){
            System.out.print(nodes.get(i).name + " ");
        }
        System.out.println();
    }
    public void getnodesinfo(){
        for(int i=0;i<nodes.size();i++){
            System.out.println(nodes.get(i).name);
            System.out.println(nodes.get(i).getInString());
            System.out.println(nodes.get(i).getOutString());
        }
    }
    public void findhigh(String a, String b){
        int k = this.containsnodeindex(a);
        int l= this.containsnodeindex(b);
        int cost =nodes.get(k).findshorterway(nodes.get(l));
        System.out.println("The shorter way have a cost of: " +cost);
        if(nodes.get(k).srtwayshr.get(cost) == null ){
            //System.out.println("null");
            if(cost <=0){
                System.out.println("The route don't exist");
            }else{
                System.out.println("The route is: " + a +" "+ b);
            }
        }else{
            String line = nodes.get(k).srtwayshr.get(cost);
            if(cost <=0){
                System.out.println("The route don't exist");
            }else{
                System.out.println("The route is: " + line +" "+b );
            }
            
        }
        nodes.get(k).srtwayshr.clear();
    }
    public void findlow(String a, String b){
        int k = this.containsnodeindex(a);
        int l= this.containsnodeindex(b);
        int cost =nodes.get(k).findlongestway(nodes.get(l));
        System.out.println("The longest way have a cost of: " +cost);
        if(nodes.get(k).srtwaylon.get(cost) == null ){
            if(cost <= 0){
                System.out.println("The route don't exist");
            }else{
                System.out.println("The route is: " + a +" "+ b);
            }
        }else{            
            String line = nodes.get(k).srtwaylon.get(cost);
            if(cost <=0){
                System.out.println("The route don't exist");
            }else{
                System.out.println("The route is: " + line +" "+b );
            }
            //System.out.println("The route is: " + line +" "+b);
        }
        nodes.get(k).srtwaylon.clear();
    }
}